package com.sda.fundamentals;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        System.out.println("\t\t\t\t\t\tMENU");
        System.out.println("1. Calculate the perimeter of a Circle");
        System.out.println("2. Check if your BMI is optimal or not");
        System.out.println("3. Solve the quadratic equation");
        System.out.println("4. Display all numbers");
        System.out.println("5. Display prime numbers");
        System.out.println("6. Exit");
        System.out.print("\nChoose an option from the menu: ");
        Scanner scanner = new Scanner(System.in);
        int option = scanner.nextInt();
        switch (option) {
            case 1:
                calculatePerimeterOfACircle();
                break;
            case 2:
                isBMIOptimal();
                break;
            case 3:
                solveQuadraticEquation();
                break;
            case 4:
                displayNumbers();
                break;
            case 5:
                displayPrimeNumbers();
                break;
            case 6:
                System.exit(1);
            default:
                System.out.println("You didn't choose a valid option.");
        }

    }

    // perimeter = pi*D, where D is the diameter
    private static void calculatePerimeterOfACircle() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter the diameter of the circle (float): ");
        float diameter = scanner.nextFloat();
        double perimeter = Math.PI * diameter;
        System.out.printf("%nThe perimeter of the circle with diameter %5.2f is %5.2f%n", diameter, perimeter);
    }

    // calculate BMI and checking if it's optimal or not
    private static void isBMIOptimal() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter the weight (kilograms)[float]: ");
        float weight = scanner.nextFloat();
        System.out.println("Enter the height (centimeters)[int]: ");
        int heightCm = scanner.nextInt();
        float heightM = heightCm / 100f;
        float bmi = weight / (heightM * heightM);
        if (bmi >= 18.5 && bmi <= 24.9) {
            System.out.println("BMI optimal");
        } else {
            System.out.println("BMI not optimal");
        }
    }

    // ax^2 + bx + c = 0; delta = b^2 - 4ac;
    private static void solveQuadraticEquation() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter the coefficients of the quadratic equation => they should be integer numbers");
        System.out.print("a = ");
        int a = scanner.nextInt();
        System.out.print("\nb = ");
        int b = scanner.nextInt();
        System.out.print("\nc = ");
        int c = scanner.nextInt();

        int delta = (b * b) - (4 * a * c);
        if (delta < 0) {
            System.out.println("Delta negative");
            System.exit(1);
        }
        double x1 = (-b - Math.sqrt(delta)) / 2 * a;
        double x2 = (-b + Math.sqrt(delta)) / 2 * a;

        System.out.printf("%nThe roots of quadratic equation are: x1 = %5.2f and x2 = %5.2f%n", x1, x2);
    }

    private static void displayNumbers() {
        Scanner scanner = new Scanner(System.in);
        System.out.print("\nEnter a positive integer: ");
        int number = scanner.nextInt();

        if (number < 0) {
            System.out.println("You entered a negative number!");
            System.exit(1);
        }

        for (int i = 1; i <= number; i++) {
            if (i % 3 == 0 && i % 7 == 0) {
                System.out.println("Fizz buzz");
            } else if (i % 3 == 0) {
                System.out.println("Fizz");
            } else if (i % 7 == 0) {
                System.out.println("Buzz");
            } else {
                System.out.println(i);
            }
        }
    }

    private static void displayPrimeNumbers() {
        Scanner scanner = new Scanner(System.in);
        System.out.print("\nEnter a positive number: ");
        int number = scanner.nextInt();

        if (number < 0) {
            System.out.println("You entered a negative number");
            System.exit(1);
        }

        System.out.printf("%nFind all prime numbers until %d%n", number);
        System.out.println("2"); // print the first prime number
        for (int i = 3; i < number; i++) {
            if (isPrime(i)) {
                System.out.println(i);
            }
        }

    }

    private static boolean isPrime(int number) {
        for (int i = 2; i <= Math.sqrt(number); i++) {
            if (number % i == 0) {
                return false;
            }
        }
        return true;
    }

}